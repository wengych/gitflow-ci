# This is a sample of gitlab-ci integrated with gitflow.

    Play Gitlab-CI with gitflow

---

This repository is a showcase on gitflow that integrated with CI by Gitlab-CI. 

All pipelines described in one .gitlab-ci.yml located in develop branch, and builds are triggered by branch name automatically.


Tech stacks:
> * Java & Kotlin & Maven.
> * Build on top of Docker.
> * Packaged as rpm for CentOS or other rpm-based systems.


Refer:
> * Create your own yum repository: https://wiki.centos.org/HowTos/CreateLocalRepos